from models.user import User
from utils.enums import UserType


def populating_mocked_base():
    user_adm = User(user_id='id_administrador',
                    username='user_administrador',
                    email='user_administrador@test.com',
                    user_type=UserType.administrador.name)
    user_adm.set_password('pass_test_user_administrador')
    user_adm.save()

    user_trd = User(user_id='id_triador',
                    username='user_triador',
                    email='triador@test.com',
                    user_type=UserType.triador.name)
    user_trd.set_password('pass_test_uid_triador')
    user_trd.save()

    user_fld = User(user_id='id_finalizador',
                    username='user_finalizador',
                    email='finalizador@test.com',
                    user_type=UserType.finalizador.name)
    user_fld.set_password('pass_test_uid_finalizador')
    user_fld.save()