import unittest

from mongoengine import connect

from tests.utils import populating_mocked_base


class ProcessApiSetupTest(unittest.TestCase):
    connect('mongoengine', host='mongomock://localhost')
    populating_mocked_base()

