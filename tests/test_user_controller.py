from controllers.user_controller import UserController
from models.user import User
from tests.setup_test import ProcessApiSetupTest
from utils.enums import UserType


class UserTestController(ProcessApiSetupTest):

    def test_create_user(self):
        user_controller = UserController()
        incoming_data = {'requesting_user_id': 'id_administrador',
                         'username': 'user_test',
                         'email': 'user_test@test.com',
                         'password': 'pass_test',
                         'user_type': UserType.administrador.name}
        user_id = user_controller.create_user(incoming_data)
        user = User.objects(user_id=user_id).first()
        self.assertEqual(incoming_data.get('username'), user.username)

    def test_dont_save_user_type_not_is_not_administrador(self):
        user_controller = UserController()
        incoming_data = {'requesting_user_id': 'id_finalizador',
                         'username': 'user_test',
                         'email': 'user_test@test.com',
                         'password': 'pass_test',
                         'user_type': UserType.triador.name}
        with self.assertRaises(PermissionError):
            user_controller.create_user(incoming_data)

    def test_delete_user(self):
        user_controller = UserController()
        incoming_data = {'requesting_user_id': 'id_administrador',
                         'username': 'user_test_d',
                         'email': 'user_test_d@test.com',
                         'password': 'pass_test',
                         'user_type': UserType.administrador.name}
        user_id = user_controller.create_user(incoming_data)
        user = User.objects(user_id=user_id).first()
        self.assertEqual(incoming_data.get('username'), user.username)

        self.assertTrue(user_controller.delete_user({'requesting_user_id': 'id_administrador', 'user_id': user_id}))
        self.assertIsNone(User.objects(user_id=user_id).first())
        self.assertIsNone(User.objects(user_id=user_id).first())

    def test_get_user(self):
        user_controller = UserController()
        incoming_data = {'requesting_user_id': 'id_administrador',
                         'username': 'user_test_g',
                         'email': 'user_test_g@test.com',
                         'password': 'pass_test',
                         'user_type': UserType.administrador.name}
        user_id = user_controller.create_user(incoming_data)

        user = user_controller.get_user({'requesting_user_id': 'id_administrador', 'user_id': user_id})
        self.assertEqual(incoming_data.get('username'), user.username)

    def test_updater_user(self):
        user_controller = UserController()
        incoming_data = {'requesting_user_id': 'id_administrador',
                         'username': 'user_test_u',
                         'email': 'user_test_u@test.com',
                         'password': 'pass_test',
                         'user_type': UserType.administrador.name}
        user_id = user_controller.create_user(incoming_data)

        user = user_controller.get_user({'requesting_user_id': 'id_administrador', 'user_id': user_id})
        self.assertEqual(incoming_data.get('username'), user.username)

        user_controller.update_user({'requesting_user_id': 'id_administrador',
                                     'user_id': user_id,
                                     'username': 'test_user',
                                     'email': 'test_user@test.com',
                                     'user_type': UserType.administrador.name
                                     })

        user = user_controller.get_user({'requesting_user_id': 'id_administrador', 'user_id': user_id})
        self.assertNotEqual(incoming_data.get('username'), user.username)
        self.assertEqual('test_user', user.username)

