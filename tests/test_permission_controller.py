from controllers.permission_controller import PermissionController
from tests.setup_test import ProcessApiSetupTest


class TestPermissionController(ProcessApiSetupTest):

    def test_verify_user_not_is_administrator(self):
        with self.assertRaises(PermissionError):
            PermissionController.verify_user_is_administrator('id_finalizador')

    def test_verify_user_not_is_triador(self):
        with self.assertRaises(PermissionError):
            PermissionController.verify_user_is_triador('id_finalizador')

    def test_verify_user_not_is_finalizador(self):
        with self.assertRaises(PermissionError):
            PermissionController.verify_user_is_finalizador('id_triador')

    def test_verify_user_not_is_finalizador_or_triador(self):
        with self.assertRaises(PermissionError):
            PermissionController.verify_user_is_finalizador_or_triador('id_administrador')
