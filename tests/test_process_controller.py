from controllers.process_controller import ProcessController
from models.process import Process
from tests.setup_test import ProcessApiSetupTest


class TestProcessController(ProcessApiSetupTest):

    def test_create_process(self):
        process_controller = ProcessController()
        incoming_data = {'requesting_user_id': 'id_triador',
                         'title': 'title_test',
                         'description': 'description_test',
                         'creator_user': 'id_triador'}
        process_id = process_controller.create_process(incoming_data)
        process = Process.objects(process_id=process_id).first()
        self.assertEqual(incoming_data.get('title'), process.title)

    def test_delete_process(self):
        process_controller = ProcessController()
        incoming_data = {'requesting_user_id': 'id_triador',
                         'title': 'title_test',
                         'description': 'description_test',
                         'creator_user': 'id_triador'}
        process_id = process_controller.create_process(incoming_data)
        process = Process.objects(process_id=process_id).first()
        self.assertEqual(incoming_data.get('title'), process.title)
        self.assertTrue(process_controller.delete_process({'requesting_user_id': 'id_triador',
                                                           'process_id': process_id}))
        self.assertIsNone(Process.objects(process_id=process_id).first())

    def test_get_process(self):
        process_controller = ProcessController()
        incoming_data = {'requesting_user_id': 'id_triador',
                         'title': 'title_test',
                         'description': 'description_test',
                         'creator_user': 'id_triador'}
        process_id = process_controller.create_process(incoming_data)

        process = process_controller.get_process({'requesting_user_id': 'id_triador', 'process_id': process_id})
        self.assertEqual(incoming_data.get('title'), process.title)

    def test_updater_process(self):
        process_controller = ProcessController()
        incoming_data = {'requesting_user_id': 'id_triador',
                         'title': 'title_test',
                         'description': 'description_test',
                         'creator_user': 'id_triador'}
        process_id = process_controller.create_process(incoming_data)

        process = process_controller.get_process({'requesting_user_id': 'id_triador', 'process_id': process_id})
        self.assertEqual(incoming_data.get('title'), process.title)

        process_controller.update_process({'requesting_user_id': 'id_triador',
                                           'process_id': process_id,
                                           'title': 'title_test_updated',
                                           'description': 'description_test',
                                           'creator_user': 'id_triador'})

        process = process_controller.get_process({'requesting_user_id': 'id_triador', 'process_id': process_id})
        self.assertEqual('title_test_updated', process.title)

    def test_not_finalize_process_no_opinion(self):
        process_controller = ProcessController()
        incoming_data = {'requesting_user_id': 'id_triador',
                         'title': 'title_test',
                         'description': 'description_test',
                         'creator_user': 'id_triador'}
        process_id = process_controller.create_process(incoming_data)
        r = process_controller.finalize_process({'requesting_user_id': 'id_finalizador',
                                                 'process_id': process_id})
        self.assertFalse(r.get('success'))

    def test_give_opinion_about_process(self):
        process_controller = ProcessController()
        incoming_data = {'requesting_user_id': 'id_triador',
                         'title': 'title_test',
                         'description': 'description_test',
                         'creator_user': 'id_triador'}
        process_id = process_controller.create_process(incoming_data)
        r = process_controller.give_opinion_process({'requesting_user_id': 'id_finalizador',
                                                     'process_id': process_id,
                                                     'opinion': 'My opinion is test the whole project'})
        self.assertTrue(r.get('success'))

    def test_not_finalize_process_no_opinion(self):
        process_controller = ProcessController()
        incoming_data = {'requesting_user_id': 'id_triador',
                         'title': 'title_test',
                         'description': 'description_test',
                         'creator_user': 'id_triador'}
        process_id = process_controller.create_process(incoming_data)
        r = process_controller.finalize_process({'requesting_user_id': 'id_finalizador',
                                                 'process_id': process_id})
        self.assertFalse(r.get('success'))

    def test_finalize_process(self):
        process_controller = ProcessController()
        incoming_data = {'requesting_user_id': 'id_triador',
                         'title': 'title_test',
                         'description': 'description_test',
                         'creator_user': 'id_triador'}
        process_id = process_controller.create_process(incoming_data)
        r = process_controller.give_opinion_process({'requesting_user_id': 'id_finalizador',
                                                     'process_id': process_id,
                                                     'opinion': 'My opinion is test the whole project'})
        self.assertTrue(r.get('success'))
        r = process_controller.finalize_process({'requesting_user_id': 'id_finalizador',
                                                 'process_id': process_id})
        self.assertTrue(r.get('success'))
        process = process_controller.get_process({'requesting_user_id': 'id_triador',
                                                  'process_id': r.get('process_id')})
        self.assertEqual(process.user_closing, 'id_finalizador')
        self.assertIsNotNone(process.closing_date)

    def test_assign_users_process(self):
        process_controller = ProcessController()
        incoming_data = {'requesting_user_id': 'id_triador',
                         'title': 'title_test',
                         'description': 'description_test',
                         'creator_user': 'id_triador'}
        process_id = process_controller.create_process(incoming_data)
        r = process_controller.give_opinion_process({'requesting_user_id': 'id_finalizador',
                                                     'process_id': process_id,
                                                     'opinion': 'My opinion is test the whole project'})
        self.assertTrue(r.get('success'))
        r = process_controller.assign_users_process({'requesting_user_id': 'id_triador',
                                                     'process_id': process_id,
                                                     'assign_users': ['id_triador',
                                                                      'id_finalizador',
                                                                          'id_administrador']})
        self.assertTrue(r.get('success'))
        process = process_controller.get_process({'requesting_user_id': 'id_triador',
                                                  'process_id': r.get('process_id')})
        self.assertEqual(3, len(process.assigned_users))
