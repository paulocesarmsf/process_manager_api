from enum import Enum


class UserType(Enum):
    administrador = 1
    triador = 2
    finalizador = 3


class ProcessStatus(Enum):
    pending = 1
    done = 2