from models.user import User
from utils.enums import UserType


def insert_first_user_adiministrador():
    user_adm = User(user_id='id_administrador',
                    username='user_administrador',
                    email='user_administrador@test.com',
                    user_type=UserType.administrador.name)
    user_adm.set_password('pass_test_user_administrador')
    user_adm.save()
