## Prerequisite

- **Docker** - https://docs.docker.com/install/linux/docker-ce/ubuntu/
- **Docker Compose** - https://docs.docker.com/compose/install/

## Steps for run

1. git clone git@bitbucket.org:paulocesarmsf/process_manager_api.git
2. docker-compose down && docker-compose up -d (at root the project)

## Steps for run local

1. git clone git@bitbucket.org:paulocesarmsf/process_manager_api.git
2. pip install -r requirements.txt (at root the project)
3. docker run --name mongodb -p 27017:27017 -d mongo


## Run tests
1. nose2 -v (at root the project)

## Route Docs
You can access Postman routes https://www.getpostman.com/collections/66e32e055de88c95f9f8