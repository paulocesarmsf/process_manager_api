import uuid
import datetime

from mongoengine import Document, StringField, ListField, DateTimeField

from utils.enums import ProcessStatus


class Process(Document):
    process_id = StringField(default=lambda: uuid.uuid4().hex, max_length=40, required=True)
    title = StringField(required=True)
    description = StringField(required=True)
    assigned_users = ListField(default=[])
    creator_user = StringField(required=True)
    opinions = ListField(default=[])
    status = StringField(default=ProcessStatus.pending.name)
    timestamp = DateTimeField(default=datetime.datetime.now())
    last_update_date = DateTimeField()
    user_closing = StringField()
    closing_date = DateTimeField()
    user_triador = StringField
