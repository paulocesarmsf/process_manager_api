import uuid

from mongoengine import Document, StringField, DateTimeField
from flask_login import UserMixin
import datetime
from werkzeug.security import generate_password_hash, check_password_hash

from utils.enums import UserType


class User(Document, UserMixin):
    user_id = StringField(default=lambda: uuid.uuid4().hex, max_length=40, required=True)
    username = StringField(required=True, unique=True)
    email = StringField(required=True, unique=True)
    password_hash = StringField(required=True)
    user_type = StringField(default=UserType.triador.name)
    timestamp = DateTimeField(default=datetime.datetime.now())

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
