import logging

from models.user import User
from utils.enums import UserType


class PermissionController(object):

    @staticmethod
    def verify_user_is_administrator(user_id):
        user = User.objects(user_id=user_id).first()
        if not user.user_type == UserType.administrador.name:
            logging.error('Permission denied, user {user_id} not is administrator'.format(user_id=user_id))
            raise PermissionError

    @staticmethod
    def verify_user_is_triador(user_id):
        user = User.objects(user_id=user_id).first()
        if not user.user_type == UserType.triador.name:
            logging.error('Permission denied, user {user_id} not is triador'.format(user_id=user_id))
            raise PermissionError

    @staticmethod
    def verify_user_is_finalizador(user_id):
        user = User.objects(user_id=user_id).first()
        if not user.user_type == UserType.finalizador.name:
            logging.error('Permission denied, user {user_id} not is finalizador'.format(user_id=user_id))
            raise PermissionError

    @staticmethod
    def verify_user_is_finalizador_or_triador(user_id):
        permissions = [UserType.finalizador.name, UserType.triador.name]
        user = User.objects(user_id=user_id).first()
        if not user.user_type in permissions:
            logging.error('Permission denied, user {user_id} not is finalizador or triador'.format(user_id=user_id))
            raise PermissionError
