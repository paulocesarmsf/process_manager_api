from mongoengine import NotUniqueError

from controllers.permission_controller import PermissionController
from models.user import User


class UserController(object):

    def create_user(self, incoming_data):
        try:
            PermissionController.verify_user_is_administrator(incoming_data.get('requesting_user_id'))
            user = User(username=incoming_data.get('username'),
                        email=incoming_data.get('email'),
                        user_type=incoming_data.get('user_type'))
            user.set_password(incoming_data.get('password'))
            user.save()
            return user.user_id
        except NotUniqueError as e:
            raise e
        except Exception as e:
            raise e

    def delete_user(self, incoming_data):
        try:
            PermissionController.verify_user_is_administrator(incoming_data.get('requesting_user_id'))
            user = User.objects(user_id=incoming_data.get('user_id')).first()
            if user:
                user.delete()
                return True
            return False
        except Exception as e:
            raise e

    def get_user(self, incoming_data):
        try:
            PermissionController.verify_user_is_administrator(incoming_data.get('requesting_user_id'))
            return User.objects(user_id=incoming_data.get('user_id')).first()
        except Exception as e:
            raise e

    def get_all_user(self, incoming_data):
        try:
            PermissionController.verify_user_is_administrator(incoming_data.get('requesting_user_id'))
            return User.objects().all()
        except Exception as e:
            raise e

    def update_user(self, incoming_data):
        try:
            PermissionController.verify_user_is_administrator(incoming_data.get('requesting_user_id'))
            user = User.objects(user_id=incoming_data.get('user_id')).first()
            if user:
                user.update(set__username=incoming_data.get('username', user.username),
                            set__email=incoming_data.get('email', user.email),
                            set__user_type=incoming_data.get('user_type', user.user_type))
                if incoming_data.get('password'):
                    user.set_password(incoming_data.get('password'))
                return user.user_id
            return False
        except Exception as e:
            raise e
