from mongoengine import NotUniqueError

from controllers.permission_controller import PermissionController
from models.process import Process
from utils.enums import ProcessStatus
import logging
import datetime


class ProcessController(object):

    def create_process(self, incoming_data):
        try:
            PermissionController.verify_user_is_triador(incoming_data.get('requesting_user_id'))
            process = Process(title=incoming_data.get('title'),
                              description=incoming_data.get('description'),
                              assigned_users=incoming_data.get('assigned_users', []),
                              creator_user=incoming_data.get('requesting_user_id'),
                              opinions=incoming_data.get('opnions', []),
                              status=incoming_data.get('status', ProcessStatus.pending.name))
            process.save()
            return process.process_id
        except NotUniqueError as e:
            raise e
        except Exception as e:
            raise e

    def delete_process(self, incoming_data):
        try:
            PermissionController.verify_user_is_triador(incoming_data.get('requesting_user_id'))
            process = Process.objects(process_id=incoming_data.get('process_id')).first()
            if process:
                process.delete()
                return True
            return False
        except Exception as e:
            raise e

    def get_process(self, incoming_data):
        try:
            PermissionController.verify_user_is_triador(incoming_data.get('requesting_user_id'))
            return Process.objects(process_id=incoming_data.get('process_id')).first()
        except Exception as e:
            raise e

    def get_all_process(self, incoming_data):
        try:
            PermissionController.verify_user_is_triador(incoming_data.get('requesting_user_id'))
            return Process.objects().all()
        except Exception as e:
            raise e

    def update_process(self, incoming_data):
        try:
            PermissionController.verify_user_is_triador(incoming_data.get('requesting_user_id'))
            process = Process.objects(process_id=incoming_data.get('process_id')).first()
            if process:
                process.update(set__title=incoming_data.get('title', process.title),
                               set__description=incoming_data.get('description', process.description),
                               set__assigned_users=incoming_data.get('assigned_users', process.assigned_users),
                               set__opinions=incoming_data.get('opnions', process.opinions),
                               set__status=incoming_data.get('status', process.status))
                return process.process_id
            return False
        except Exception as e:
            raise e

    def get_procces_by_not_opnion(self, incoming_data):
        try:
            PermissionController.verify_user_is_finalizador(incoming_data.get('requesting_user_id'))
            return Process.objects(opinions__size=0).all()
        except Exception as e:
            raise e

    def finalize_process(self, incoming_data):
        try:
            PermissionController.verify_user_is_finalizador(incoming_data.get('requesting_user_id'))
            process_id = incoming_data.get('process_id')
            process = Process.objects(process_id=process_id).first()
            if process:
                if process.opinions:
                    process.update(set__status=ProcessStatus.done.name,
                                   set__user_closing=incoming_data.get('requesting_user_id'),
                                   set__closing_date=datetime.datetime.now())
                    return {'success': True, 'process_id': process.process_id}
                return {'success': False,
                        'message': 'Process with {process_id} can not be finalized '
                                   'without having an opinion'.format(process_id=process_id)}
            logging.error('Process {process_id} with not found'.format(process_id=process_id))
            return {'success': False, 'message': 'Process with {process_id} not found'.format(process_id=process_id)}
        except Exception as e:
            raise e

    def give_opinion_process(self, incoming_data):
        try:
            PermissionController.verify_user_is_finalizador_or_triador(incoming_data.get('requesting_user_id'))
            process_id = incoming_data.get('process_id')
            process = Process.objects(process_id=process_id).first()
            if process:
                process.opinions.append({'opinions': incoming_data.get('opinion'),
                                         'user_id': incoming_data.get('requesting_user_id')})
                process.last_update_date = datetime.datetime.now()
                process.save()
                return {'success': True, 'process_id': process.process_id}
            logging.error('Process {process_id} with not found'.format(process_id=process_id))
            return {'success': False, 'message': 'Process with {process_id} not found'.format(process_id=process_id)}
        except Exception as e:
            raise e

    def assign_users_process(self, incoming_data):
        try:
            PermissionController.verify_user_is_triador(incoming_data.get('requesting_user_id'))
            process_id = incoming_data.get('process_id')
            process = Process.objects(process_id=process_id).first()
            if process:
                for user_id in incoming_data.get('assign_users'):
                    process.assigned_users.append(user_id)
                process.user_triador = incoming_data.get('requesting_user_id')
                process.last_update_date = datetime.datetime.now()
                process.save()
                return {'success': True, 'process_id': process.process_id}
            logging.error('Process {process_id} with not found'.format(process_id=process_id))
            return {'success': False, 'message': 'Process with {process_id} not found'.format(process_id=process_id)}
        except Exception as e:
            raise e
