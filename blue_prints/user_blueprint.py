import json

from flask import Blueprint, request, jsonify
from mongoengine import NotUniqueError

from controllers.user_controller import UserController

user_blueprint = Blueprint('user_blueprint', __name__)
user_controller = UserController()


@user_blueprint.route('/spec', methods=['GET'])
def index():
    return 'user blueprint ok!'


@user_blueprint.route('/save', methods=['POST'])
def create_user():
    try:
        requesting_user_id = request.headers.get('requesting_user_id')
        if requesting_user_id:
            incoming_data = request.get_json()
            incoming_data.update({'requesting_user_id': requesting_user_id})
            user_id = user_controller.create_user(incoming_data)
            return jsonify({'success': True, 'user_id': user_id}), 200
        return jsonify({'success': False, 'message': 'requesting_user_id not informed'}), 400
    except NotUniqueError:
        return jsonify({'success': False, 'message': 'Not unique error'}), 400
    except Exception:
        return jsonify({'success': False, 'message': 'unexpected error'}), 500
    except PermissionError:
        return jsonify({'success': False, 'message': 'user not adiministrador'}), 403


@user_blueprint.route('/delete/<id>', methods=['DELETE'])
def delete_user(id):
    try:
        requesting_user_id = request.headers.get('requesting_user_id')
        if requesting_user_id:
            requesting_user_id = request.headers.get('requesting_user_id')
            deleted = user_controller.delete_user({'requesting_user_id': requesting_user_id, 'user_id': id})
            if deleted:
                return jsonify({'success': True}), 200
            return jsonify({'success': False}), 200
        return jsonify({'success': False, 'message': 'requesting_user_id not informed'}), 400
    except Exception:
        return jsonify({'success': False, 'message': 'unexpected error'}), 500
    except PermissionError:
        return jsonify({'success': False, 'message': 'user not adiministrador'}), 403


@user_blueprint.route('/update', methods=['PUT'])
def update_user():
    try:
        requesting_user_id = request.headers.get('requesting_user_id')
        if requesting_user_id:
            incoming_data = request.get_json()
            incoming_data.update({'requesting_user_id': requesting_user_id})
            user_id = user_controller.update_user(incoming_data)
            return jsonify({'success': True, 'user_id': user_id}), 200
        return jsonify({'success': False, 'message': 'requesting_user_id not informed'}), 400
    except NotUniqueError:
        return jsonify({'success': False, 'message': 'Not unique error'}), 400
    except Exception:
        return jsonify({'success': False, 'message': 'unexpected error'}), 500
    except PermissionError:
        return jsonify({'success': False, 'message': 'user not adiministrador'}), 403


@user_blueprint.route('/find/<id>', methods=['GET'])
def get_user(id):
    try:
        requesting_user_id = request.headers.get('requesting_user_id')
        if requesting_user_id:
            user = user_controller.get_user({'requesting_user_id': requesting_user_id, 'user_id': id})
            if user:
                return jsonify({'success': True, 'data': json.loads(user.to_json())})
            return jsonify({'success': True, 'message': "User not found"})
        return jsonify({'success': False, 'message': 'requesting_user_id not informed'}), 400
    except Exception:
        return jsonify({'success': False, 'message': 'unexpected error'}), 500
    except PermissionError:
        return jsonify({'success': False, 'message': 'user not adiministrador'}), 403


@user_blueprint.route('/find/all', methods=['GET'])
def get_all_users():
    try:
        requesting_user_id = request.headers.get('requesting_user_id')
        if requesting_user_id:
            users = user_controller.get_all_user({'requesting_user_id': requesting_user_id})
            if users:
                return jsonify({'success': True, 'data': json.loads(users.to_json())})
            return jsonify({'success': True, 'message': "none user saved"})
        return jsonify({'success': False, 'message': 'requesting_user_id not informed'}), 400
    except Exception:
        return jsonify({'success': False, 'message': 'unexpected error'}), 500
    except PermissionError:
        return jsonify({'success': False, 'message': 'user not adiministrador'}), 403
