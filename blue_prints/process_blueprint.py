import json

from flask import Blueprint, jsonify, request

from controllers.process_controller import ProcessController

process_blueprint = Blueprint('process_blueprint', __name__)
process_controller = ProcessController()


@process_blueprint.route('/spec', methods=['GET'])
def index():
    return 'process blueprint ok!'


@process_blueprint.route('/save', methods=['POST'])
def create_process():
    try:
        requesting_user_id = request.headers.get('requesting_user_id')
        if requesting_user_id:
            incoming_data = request.get_json()
            incoming_data.update({'requesting_user_id': requesting_user_id})
            process_id = process_controller.create_process(incoming_data)
            return jsonify({'success': True, 'process_id': process_id}), 200
        return jsonify({'success': False, 'message': 'requesting_user_id not informed'}), 400
    except Exception:
        return jsonify({'success': False, 'message': 'unexpected error'}), 500
    except PermissionError:
        return jsonify({'success': False, 'message': 'user not triador'}), 403


@process_blueprint.route('/delete/<id>', methods=['DELETE'])
def delete_process(id):
    try:
        requesting_user_id = request.headers.get('requesting_user_id')
        if requesting_user_id:
            requesting_user_id = request.headers.get('requesting_user_id')
            deleted = process_controller.delete_process({'requesting_user_id': requesting_user_id,
                                                         'process_id': id})
            if deleted:
                return jsonify({'success': True}), 200
            return jsonify({'success': False}), 200
        return jsonify({'success': False, 'message': 'requesting_user_id not informed'}), 400
    except Exception:
        return jsonify({'success': False, 'message': 'unexpected error'}), 500
    except PermissionError:
        return jsonify({'success': False, 'message': 'user not triador'}), 403


@process_blueprint.route('/update', methods=['PUT'])
def update_process():
    try:
        requesting_user_id = request.headers.get('requesting_user_id')
        if requesting_user_id:
            incoming_data = request.get_json()
            incoming_data.update({'requesting_user_id': requesting_user_id})
            process_id = process_controller.update_process(incoming_data)
            return jsonify({'success': True, 'user_id': process_id}), 200
        return jsonify({'success': False, 'message': 'requesting_user_id not informed'}), 400
    except Exception:
        return jsonify({'success': False, 'message': 'unexpected error'}), 500
    except PermissionError:
        return jsonify({'success': False, 'message': 'user not triador'}), 403


@process_blueprint.route('/find/<id>', methods=['GET'])
def get_process(id):
    try:
        requesting_user_id = request.headers.get('requesting_user_id')
        if requesting_user_id:
            process = process_controller.get_process({'requesting_user_id': requesting_user_id, 'process_id': id})
            if process:
                return jsonify({'success': True, 'data': json.loads(process.to_json())})
            return jsonify({'success': True, 'message': "process not found"})
        return jsonify({'success': False, 'message': 'requesting_user_id not informed'}), 400
    except Exception:
        return jsonify({'success': False, 'message': 'unexpected error'}), 500
    except PermissionError:
        return jsonify({'success': False, 'message': 'user not triador'}), 403


@process_blueprint.route('/find/all', methods=['GET'])
def get_all_process():
    try:
        requesting_user_id = request.headers.get('requesting_user_id')
        if requesting_user_id:
            process = process_controller.get_all_process({'requesting_user_id': requesting_user_id})
            if process:
                return jsonify({'success': True, 'data': json.loads(process.to_json())})
            return jsonify({'success': True, 'message': "process not found"})
        return jsonify({'success': False, 'message': 'requesting_user_id not informed'}), 400
    except Exception:
        return jsonify({'success': False, 'message': 'unexpected error'}), 500
    except PermissionError:
        return jsonify({'success': False, 'message': 'user not adiministrador'}), 403


@process_blueprint.route('/find/all/not_opnion', methods=['GET'])
def get_process_all_by_not_opnion():
    try:
        requesting_user_id = request.headers.get('requesting_user_id')
        if requesting_user_id:
            process = process_controller.get_procces_by_not_opnion({'requesting_user_id': requesting_user_id})
            if process:
                return jsonify({'success': True, 'data': json.loads(process.to_json())})
            return jsonify({'success': True, 'message': "process not found"})
        return jsonify({'success': False, 'message': 'requesting_user_id not informed'}), 400
    except Exception:
        return jsonify({'success': False, 'message': 'unexpected error'}), 500
    except PermissionError:
        return jsonify({'success': False, 'message': 'user not adiministrador'}), 403


@process_blueprint.route('/finalize', methods=['POST'])
def finalize_process():
    try:
        requesting_user_id = request.headers.get('requesting_user_id')
        if requesting_user_id:
            incoming_data = request.get_json()
            incoming_data.update({'requesting_user_id': requesting_user_id})
            process_controller.finalize_process(incoming_data)
            return jsonify({'success': True}), 200
        return jsonify({'success': False, 'message': 'requesting_user_id not informed'}), 400
    except Exception:
        return jsonify({'success': False, 'message': 'unexpected error'}), 500
    except PermissionError:
        return jsonify({'success': False, 'message': 'user not triador'}), 403


@process_blueprint.route('/give_opnion', methods=['POST'])
def give_opnion_process():
    try:
        requesting_user_id = request.headers.get('requesting_user_id')
        if requesting_user_id:
            incoming_data = request.get_json()
            incoming_data.update({'requesting_user_id': requesting_user_id})
            process_controller.give_opinion_process(incoming_data)
            return jsonify({'success': True}), 200
        return jsonify({'success': False, 'message': 'requesting_user_id not informed'}), 400
    except Exception:
        return jsonify({'success': False, 'message': 'unexpected error'}), 500
    except PermissionError:
        return jsonify({'success': False, 'message': 'user not triador'}), 403


@process_blueprint.route('/assign_users', methods=['POST'])
def assign_users_process():
    try:
        requesting_user_id = request.headers.get('requesting_user_id')
        if requesting_user_id:
            incoming_data = request.get_json()
            incoming_data.update({'requesting_user_id': requesting_user_id})
            process_id = process_controller.assign_users_process(incoming_data)
            return jsonify({'success': True}), 200
        return jsonify({'success': False, 'message': 'requesting_user_id not informed'}), 400
    except Exception:
        return jsonify({'success': False, 'message': 'unexpected error'}), 500
    except PermissionError:
        return jsonify({'success': False, 'message': 'user not triador'}), 403
