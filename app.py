from flask import Flask

from blue_prints.process_blueprint import process_blueprint
from blue_prints.user_blueprint import user_blueprint
from mongo_database import configure_mongo
from utils.utils import insert_first_user_adiministrador

app = Flask(__name__)
app.register_blueprint(user_blueprint, url_prefix='/user')
app.register_blueprint(process_blueprint, url_prefix='/process')


@app.route('/spec', methods=['GET'])
def index():
    return 'ok'


if __name__ == '__main__':
    configure_mongo()
    insert_first_user_adiministrador()
    app.run(host='0.0.0.0', port=5000)
